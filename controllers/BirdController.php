<?php

namespace app\controllers;

use Yii;
use app\models\Bird;
use app\models\SearchBird;
use app\models\Image;
use app\models\Sound;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadForm;
use yii\web\UploadedFile;
use app\models\LoginForm;
use yii\web\Response;

/**
 * BirdController implements the CRUD actions for Bird model.
 */
class BirdController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bird models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchBird();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bird model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Bird model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->getIsGuest()){
            Yii::$app->user->loginRequired();
        }
        else{
            $model = new Bird();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $addNum = $_POST["addNum"];
                $fname = array();
                $name = array();
                $ftype = array();
                for($i = 0 ;$i < $addNum; $i++){
                    $fname[$i] = $_POST["fnameStr_".(string)($i+1)];
                    $name[$i] = $_POST["nameStr_".(string)($i+1)];
                    $ftype[$i] = $_POST["ftype_".(string)($i+1)];
                    if(!strncmp($ftype[$i],"image",5)){
						$modelDB = new Image();
                    }
                    else if(!strncmp($ftype[$i],"audio",5)){
                        $modelDB = new Sound();
                    }
                    else{
                        Yii::$app->session->setFlash('InvalidFile');
                        $this -> refresh();
                    }
                    $modelDB -> name = $name[$i];
                    $modelDB -> filename = $fname[$i];
                    $modelDB -> bird_id = $model -> id;
                    $modelDB -> save();

                }
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Bird model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        
        if(Yii::$app->user->getIsGuest()){
		    Yii::$app->user->loginRequired();
        }
        else{
            $model = $this->findModel($id);


            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $addNum = $_POST["addNum"];
                $delNum = $_POST["delNum"];
                $fname = array();
                $name = array();
                $ftype = array();
                $delFname = array();
                for($i = 0 ;$i < $addNum; $i++){
                    $fname[$i] = $_POST["fnameStr_".(string)($i+1)];
                    $name[$i] = $_POST["nameStr_".(string)($i+1)];
                    $ftype[$i] = $_POST["ftype_".(string)($i+1)];
                    if(!strncmp($ftype[$i],"image",5)){
						$modelDB = new Image();
                    }
                    else if(!strncmp($ftype[$i],"audio",5)){
                        $modelDB = new Sound();
                    }
                    else{
                        Yii::$app->session->setFlash('InvalidFile');
                        $this -> refresh();
                    }
                    $modelDB -> name = $name[$i];
                    $modelDB -> filename = $fname[$i];
                    $modelDB -> bird_id = $id;
                    $modelDB -> save();

                }
                for($i = 0 ;$i < $delNum; $i++){
                    $fname[$i] = $_POST["fnameDel_".$i];
                    $ftype[$i] = $_POST["fTypeDel_".$i];
                    if($ftype[$i]=="image"){
                        $toDel = Image::find()->where(['filename'=>$fname[$i]])->one();
                    }
                    else if($ftype[$i]=="audio"){
                        $toDel = Sound::find()->where(['filename'=>$fname[$i]])->one();
                    }
                    $toDel -> delete();

                }
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing Bird model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->getIsGuest()){
			Yii::$app->user->loginRequired();
        }
        else{
			$this->findModel($id)->delete();

            return $this->redirect(['index']);
        }
    }
	


    /**
     * Finds the Bird model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bird the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bird::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
