<?php

namespace app\controllers;
use Yii;
use app\models\TestTable;
use yii\web\UploadedFile;


class TestTableController extends \yii\web\Controller
{
    public function actionInput()
    {
	$model = new \yii\base\DynamicModel(['image']);
	$model->addRule('image','file');
	if(Yii::$app->request->isPost){
		$model->image = UploadedFile::getInstance($model,'image');
		if($model->image && $model->validate()){
			$model->image->saveAs('images/4.jpg');
			return $this->redirect(['bird/index']);
		}
		
	}       
	return $this->render('input',['model'=>$model]);
    }

}

?>
