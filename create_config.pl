#!/usr/bin/env perl

# read stats dir from argument
$configName = $ARGV[0] || die "Usage: $0 <target filename> [<template filename>]";
$templateName = $ARGV[1] || $configName . ".template";

open (FILE, $templateName) or die $!;
open (my $output, '>', $configName) or die $!;

print "configuring $configName\n";
foreach $line (<FILE>) {
	while (($match) = ($line =~ m/{{(.*?)}}/)) {
		print $match . ":";
		my $val = <STDIN>;
		chomp($val);
		if (length($val) > 0) {
			$line =~ s/{{(.*?)}}/$val/;
		}
	}
	print $output $line;	
}
