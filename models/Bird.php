<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bird".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * @property Image[] $images
 * @property Sound[] $sounds
 */
class Bird extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bird';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['name'], 'string', 'max' => 32],
            [['description'], 'string', 'max' => 65536]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['bird_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSounds()
    {
        return $this->hasMany(Sound::className(), ['bird_id' => 'id']);
    }
}
