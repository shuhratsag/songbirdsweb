<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property integer $bird_id
 * @property string $name
 * @property string $filename
 *
 * @property Bird $bird
 */
class Image extends \yii\db\ActiveRecord
{
    
/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bird_id', 'name', 'filename'], 'required'],
            [['bird_id'], 'integer'],
            [['filename'], 'string', 'max' => 256],
            [['name'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bird_id' => 'Bird ID',
            'name' => 'Name',
            'filename' => 'Filename',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBird()
    {
        return $this->hasOne(Bird::className(), ['id' => 'bird_id']);
    }
}
