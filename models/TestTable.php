<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "test_table".
 *
 * @property integer $id
 * @property string $about
 */
class TestTable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['about', 'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'about' => 'About',
        ];
    }
}
