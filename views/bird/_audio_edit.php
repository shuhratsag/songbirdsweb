<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="row audio-unit" id="<?=$model->filename ?>">
    <div class="col-md-10">
	<?php
    echo "<audio controls><source src=\"upload/files/";
	echo "{$model->filename}\" type=\"audio/mpeg\"></audio>";
    ?>
    </div>
    <div class="col-md-2">
        <?php
    echo Html::button('X', ['class' => 'btn btn-delete-aud', 'data' => ['confirm'=>'Вы уверены что хотите удалить этот звук?']]);
        ?>
    </div>
</div>
