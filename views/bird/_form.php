<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use yii\base\DynamicModel;
use yii\web\View;
use vova07\imperavi\Widget as Imperavi;

/* @var $this yii\web\View */
/* @var $model app\models\Bird */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bird-form">


    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 32])->label('Название') ?>

    <?= 
		//$form->field($model, 'description')->textInput(['maxlength' => 1024])->label('Описание')
		$form->field($model, 'description')->widget(Imperavi::className(), [
			'settings' => [
				'lang' => 'ru',
				'minHeight' => 200,
				'plugins' => [
					'clips',
					'fullscreen'
				]
			]
		]); 
	?>
	
    <!-- The fileinput-button span is used to style the file input field as button -->
    <span class="btn btn-success fileinput-button">
        <i class="glyphicon glyphicon-plus"></i>
        <span>Добавить фото или звук...</span>
        <!-- The file input field used as target for the file upload widget -->
        <input id="fileupload" type="file" name="files[]" >
    </span>
    <br>
    <br>
    <!-- The global progress bar -->
    <div id="progress" class="progress">
        <div class="progress-bar progress-bar-success"></div>
    </div>
    <!-- The container for the uploaded files -->
    <div id="files" class="files"></div>

	<div class="container">	
        <div class="row">
            <div class="col-md-4">
                
			<?php
				$image = app\models\Image::find()->where(['bird_id'=>$model->id])->orderBy('id')->all();
                if(count($image)>0){
                    echo '<h2 class="image-header">Фотографии:</h2>';
                }
				foreach($image as $value){					
					echo '<div class="thumbnail" id="'.$value->filename.'" >';
					echo Html::img('@web/upload/files/'.$value->filename,["class"=>"thmbnl"]);
					echo "<div class=\"caption\">";
					echo Html::button('X', ['id'=>'sh', 'class' => 'btn btn-delete-img']);
					echo "</div>";
					echo "</div>";
				}
			?>
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
			<?php
				$sound = app\models\Sound::find()->where(['bird_id'=>$model->id])->orderBy('id')->all();
                if(count($sound)>0){
                    echo '<h2 class="sound-header">Звуки:</h2>';
                }
				foreach($sound as $value){					
					echo '<div class="row audio-unit" id="'.$value->filename.'" >';
					echo '<div class="col-md-10">';
                    echo "<audio controls><source src=\"upload/files/";
	                echo "{$value->filename}\" type=\"audio/mpeg\"></audio>";
                    echo '</div>';
                    echo '<div class="col-md-2">';
                    echo Html::button('X', ['class' => 'btn btn-delete-aud']);
					echo "</div>";
					echo "</div>";

				}
			?>
                </div>
        </div>
		</div>
	</div>
    
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'],['id'=>'submit-button']) ?>
        <?= Html::a('Отменить', ['index'], ['class' => 'btn btn-default']); ?>
    </div>
    <?php ActiveForm::end(); ?>

     <?php
    $this->registerJsFile('assets/js/vendor/jquery.ui.widget.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('assets/js/jquery.iframe-transport.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('assets/js/jquery.fileupload.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('assets/js/jquery.fileupload-process.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('assets/js/jquery.fileupload-image.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('assets/js/jquery.fileupload-audio.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile('assets/js/jquery.fileupload-validate.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

    $this->registerJsFile('//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerCssFile('//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css',['position' => View::POS_HEAD]);
    $this->registerCssFile('css/style.css',['position' => View::POS_HEAD]);
    $this->registerCssFile('css/jquery.fileupload.css',['position' => View::POS_HEAD]);
    $this->registerJsFile('assets/js/jquery.init.js', ['position' => View::POS_END,'depends' => [\yii\web\JqueryAsset::className()]]);
    ?>
</div>
