<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="thumbnail">
<?=Html::img('@web/images/'.$model->filename)?>
<div class="caption">
<?=Html::a('X', ['image/delete-image', 'id' => $to->id, 'fname'=>$model->filename], ['class' => 'btn btn-danger', 'data' => ['confirm'=>'Вы уверены что хотите удалить эту фотографию?', 'method'=>'post']])
?>
</div>
