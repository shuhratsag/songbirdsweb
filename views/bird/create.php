<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Bird */

$this->title = 'Новая птица';
$this->params['breadcrumbs'][] = ['label' => 'Птицы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bird-create">

    <h1><?= Html::encode($this->title) ?></h1>
  

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
