<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchBird */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Птицы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bird-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Новая птица', ['create'], ['class' => 'btn btn-success']); ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['format' => 'raw', 'label' => 'Изображение', 'value' => function($data){

                    $image = app\models\Image::find()->where(['bird_id'=>$data->id])->one();
					if ($image) {
						return Html::a(Html::img('@web/upload/files/thumbnail/'.$image->filename,["class"=>"thmbnl"]), ['view','id'=>$data->id]);
					}
					else {
						return '';
					}

                }
            ],
            ['attribute' => 'name','format' => 'raw', 'label' => 'Название', 'value' => function($data){


                return Html::a($data->name, ['view','id'=>$data->id]);

                }
            ],
            ['attribute' => 'description', 'format' => 'html', 'label' => 'Описание', 'value' => function($data){
                $str = $data->description;
                $pieces = explode(" ", $str);
                if (count($pieces) > 50) {
                    $str = implode(" ", array_splice($pieces, 0, 50)) . " " . Html::a('...', ['view','id'=>$data->id]);
                }
                return $str;
            }],
            
            /*[
08
            'label'=>'Изображение',
09
            'format'=>'raw',
10
            'value' => function($data){
11
                $image = app\models\Image::find()->where(['bird_id'=>$data->id])->one();
                $url = '@web/upload/files/thumbnail/'.$image->filename;
12
                return Html::img($url,['alt'=>'yii']);
13
            }
14
        ],*/
            
            



            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
