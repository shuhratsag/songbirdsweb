<?php

use yii\helpers\Html;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Bird */

$this->title = 'Редактировать:';
$this->params['breadcrumbs'][] = ['label' => 'Птицы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="bird-update">

    <h1><?= Html::encode($this->title) ?></h1>

	
    <?= $this->render('_form', [
        'model' => $model
    ]) ?>
    
   

</div>
