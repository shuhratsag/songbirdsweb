<?php
//use Yii;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Bird */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Птицы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bird-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php echo Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                echo " ";
              echo Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены что хотите удалить эту птицу?',
                'method' => 'post',
            ],
              ]); ?>
	
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [           
            [
				'label'=>'Название',
				'value'=>$model->name
			],
            [
				'label'=>'Описание',
				'value'=>$model->description,
				'format'=>'html'
			]
        ],
    ]);
    ?>
    
    <div class="container">	
        <div class="row">
            <div class="col-md-4">
                
			<?php
				$image = app\models\Image::find()->where(['bird_id'=>$model->id])->orderBy('id')->all();
                if(count($image)>0){
                    echo '<h2 class="image-header">Фотографии:</h2>';
                }
				foreach($image as $value){					
					echo '<a href="upload/files/';
                    echo $value->filename;
                    echo '" class="thumbnail" rel="group" id="'.$value->filename.'" >';
					echo Html::img('@web/upload/files/'.$value->filename,["class"=>"thmbnl"]);
					echo "</a>";
				}
			?>
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
			<?php
				$sound = app\models\Sound::find()->where(['bird_id'=>$model->id])->orderBy('id')->all();
                if(count($sound)>0){
                    echo '<h2 class="sound-header">Звуки:</h2>';
                }
				foreach($sound as $value){					
					echo '<div class="row audio-unit" id="'.$value->filename.'" >';
					echo '<div class="col-md-12">';
                    echo "<audio controls><source src=\"upload/files/";
	                echo "{$value->filename}\" type=\"audio/mpeg\"></audio>";
                    echo '</div>';
					echo "</div>";

				}
			?>
                </div>
        </div>
		</div>
	</div>

    <?php $this->registerJsFile('http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js');
    $this->registerJsFile('assets/js/jquery.fancybox.pack.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerCssFile('css/jquery.fancybox.css',['position' => View::POS_HEAD]);
    $this->registerJs('$(document).ready(function() {$(".thumbnail").fancybox();});', View::POS_END);
    ?>
                   
</div>
