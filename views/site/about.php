<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'О проекте';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Изучай язык птиц вместе с нами!
    </p>

</div>
